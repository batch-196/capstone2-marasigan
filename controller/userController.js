const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (req, res) => {
    const hashedPw = bcrypt.hashSync(req.body.password, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

module.exports.getUsers = (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(error => res.send(error))
};

module.exports.loginUser = (req, res) => {
    User.findOne({ email: req.body.email })
        .then(foundUser => {
            if (foundUser === null) {
                return res.send({ message: "No User Found." })
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(foundUser) });
                } else {
                    return res.send({ message: "Incorrect Password" });
                }
            }
        })
};

module.exports.userLanding = (req, res) => {
    res.send("/register to register a user \n /login to get access token");
}

module.exports.getUserDetails = (req, res) => {
    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(error => res.send(error))
};

module.exports.updateUser = (req, res) => {
    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo
    }
    User.findByIdAndUpdate(req.params.userId, updates, { new: true })
        .then(userUpdate => res.send(userUpdate))
        .catch(error => res.send(error))
}

module.exports.setAdmin = (req, res) => {
    let update = {
        isAdmin: true
    };
    User.findByIdAndUpdate(req.params.userId, update, { new: true })
        .then(result => res.send(result))
        .catch(error => res.send(error))
}


module.exports.checkUserExists = (req,res) => {
        User.findOne({ $or: [{ email: req.body.email }, { mobileNo: req.body.mobileNo }] })
        .then(result => {
            if(result === null){
                return res.send(false);
            } else {
                return res.send(true)
            }
        })
        .catch(err => res.send(err));
}