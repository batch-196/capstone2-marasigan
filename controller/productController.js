const Product = require("../models/Product.js");

module.exports.addProduct = (req, res) => {

    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })

    newProduct.save()
        .then(result => res.send(result))
        .catch(error => res.send(error))
};

module.exports.getProducts = (req, res) => {
    let exclude = { 
        orders:false,
    };
    Product.find({}, exclude)
        .then(result => res.send(result))
        .catch(error => res.send(error))

};

module.exports.getActiveProducts = (req, res) => {
    let exclude = { 
        orders:false,
    };
    Product.find({ isActive: true }, exclude)
        .then(result => res.send(result))
        .catch(error => res.send(error))
};



module.exports.getSingleProduct = (req, res) => {
    let exclude = { 
        orders:  false
    };
    Product.findById(req.params.productId, exclude)
        .then(result => res.send(result))
        .catch(error => res.send(error))
};


module.exports.updateProduct = (req, res) => {

    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        isActive:req.body.isActive
    }

    Product.findByIdAndUpdate(req.params.productId, updates, { new: true })
        .then(productUpdate => res.send(productUpdate))
        .catch(error => res.send(error))
};


module.exports.archiveProduct = (req, res) => {
    let update = {
        isActive: false
    };
    Product.findByIdAndUpdate(req.params.productId, update, { new: true })
        .then(result => res.send(result))
        .catch(error => res.send(error))
}

module.exports.activateProduct = (req, res) => {
    let update = {
        isActive: true
    };
    Product.findByIdAndUpdate(req.params.productId, update, { new: true })
        .then(result => res.send(result))
        .catch(error => res.send(error))
}

module.exports.checkProductOrders = (req, res) => {
    let exclude = { 
        description: false,
        price: false,
        createdOn:false
    };

    Product.findById(req.params.productId, exclude)
        .then(result => res.send(result))
        .catch(error => res.send(error))

}

