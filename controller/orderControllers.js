const Order = require("../models/Order");
const Product = require("../models/Product");
//const User = require("../models/User");

module.exports.addOrder = async (req, res) => {

    if (req.user.isAdmin) {
        return res.send({ message: "Action Forbidden." });
    }

    let newOrder = new Order({
        userId: req.user.id,
        totalAmount: req.body.totalAmount,
        products: req.body.products
    })

    // newOrder.products.forEach(product => {
    //     Product.findById(product.productId).then(foundProduct => {
    //         if(foundProduct.isActive){
    //             console.log("true");
    //         }else{
    //             console.log("false");
    //         }
    //     })
    //     .catch(error => res.send(error))
    // })

    let orderId = await newOrder.save()
        .then(order => order.id)
        .catch(error => res.send(error))



    newOrder.products.forEach(product => {

        Product.findById(product.productId).then(foundProduct => {
            let order = {
                orderId: orderId,
                quantity: product.quantity
            }
            foundProduct.orders.push(order);
            foundProduct.save()
        })
    })

    res.send({ message: "Order Successful." })
}


module.exports.getAllOrders = (req, res) => {
    let exclude = { 
        __v:false
    };
    Order.find({}, exclude)
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

module.exports.getUserOrders = (req, res) => {

    let exclude = {
        __v: false,
        userId: false,
        products: { _id: false }
    };
    Order.find({ userId: req.user.id }, exclude)
        .then(result => res.send(result))
        .catch(err => res.send(err))
}


module.exports.viewOrder = (req, res) => {
    let exclude = {
        userId: false,
        purchasedOn: false,
        totalAmount: false,
        __v: false
    };

    Order.findById(req.params.orderId, exclude)
        .then(result => res.send(result))
        .catch(error => res.send(error))
}