const express = require("express");
const mongoose = require("mongoose");
const app = express();
app.use(express.json())
const port = process.env.PORT || 4000;
const cors = require('cors');

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.k6h2z.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connected to MongoDB"));

app.use(cors());

const userRoutes = require('./routes/userRoutes.js');
app.use('/user', userRoutes);

const productRoutes = require('./routes/productRoutes.js');
app.use('/products', productRoutes);

const orderRoutes = require('./routes/orderRoutes.js');
app.use('/orders', orderRoutes);

app.listen(port, () => console.log(`Server running at localhost:4000`));