const express = require("express");
const router = express.Router();

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const userControllers = require("../controller/userController.js")

//register a user
router.post('/register', userControllers.registerUser)

//get a list of all registered users
router.get('/users', verify, verifyAdmin, userControllers.getUsers)

//existing user login
router.post('/login', userControllers.loginUser)

//User landing, this is where the user is given the options available to them
router.get('/', userControllers.userLanding)
router.post('/', userControllers.userLanding)

router.get('/getUserDetails', verify, userControllers.getUserDetails)

router.post('/checkUserExists',userControllers.checkUserExists);

router.put("/updateUser/:userId", verify, verifyAdmin, userControllers.updateUser);

router.delete("/setAdmin/:userId", verify, verifyAdmin, userControllers.setAdmin);

module.exports = router;