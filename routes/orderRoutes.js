const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const { verify, verifyAdmin } = auth;

const orderControllers = require("../controller/orderControllers.js");

//Adds an Order
router.post('/addOrder', verify, orderControllers.addOrder);

//Gets All Orders
router.get('/getOrders', verify, verifyAdmin, orderControllers.getAllOrders);

//Gets User Orders
router.get('/getUserOrders/', verify, orderControllers.getUserOrders);

//View Products in the Order
router.get('/viewOrder/:orderId', verify, orderControllers.viewOrder)

module.exports = router;