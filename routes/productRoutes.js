const express = require("express");
const router = express.Router();

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const productControllers = require("../controller/productController.js")


router.post('/addProduct', verify, verifyAdmin, productControllers.addProduct)

//All Products Inactive or Active
router.get('/',productControllers.getProducts)

//All Active Products
router.get('/active', productControllers.getActiveProducts)

//Views A Specific Products Details
router.get('/getSingleProduct/:productId', productControllers.getSingleProduct)

//Updates Product Details
router.put("/updateProduct/:productId", verify, verifyAdmin, productControllers.updateProduct);

// Archives a Product
router.delete("/archiveProduct/:productId", verify, verifyAdmin, productControllers.archiveProduct);

// Activates a Product
router.delete("/activateProduct/:productId", verify, verifyAdmin, productControllers.activateProduct);

//Checks all Orders of the Product
router.get('/checkProductOrders/:productId', productControllers.checkProductOrders)

module.exports = router;